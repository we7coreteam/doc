===== 连接其它数据库=====
操作微擎系统数据库使用系统封装好的 **pdo_xxx** 相关函数，如果您想同时连接操作其它数据库可以使用以下方法，例如：

<code php>
$other_database = array(
	'host' => '192.168.1.2', //数据库IP或是域名
	'username' => 'root', // 数据库连接用户名
	'password' => '123456', // 数据库连接密码
	'database' => 'discuz', // 数据库名
	'port' => 3306, // 数据库连接端口
	'tablepre' => 'pre_', // 表前缀，如果没有前缀留空即可
	'charset' => 'utf8', // 数据库默认编码
	'pconnect' => 0, // 是否使用长连接
);
$other_db = new DB($other_database);
//查询uid为1的会员信息
$member = $discuz_db->get('common_member', array('uid' => 1));
//更新uid为1的会员信息
$other_db->update('common_member', array('username' => 'admin888'), array('uid' => 1));
//其它使用方法与pdo_xxx相关函数相同
</code>

===== 连接其它主从数据库=====
如果要连接的数据库有主从分离，则构造连接 **config** 的时候，参考系统数据库连接配置即可，例如：

<code php>
$other_database['master']['host'] = '192.168.1.12';
$other_database['master']['username'] = 'root';
$other_database['master']['password'] = '123456';
$other_database['master']['port'] = '3306';
$other_database['master']['database'] = 'discuz';
$other_database['master']['charset'] = 'utf8';
$other_database['master']['pconnect'] = 0;
$other_database['master']['tablepre'] = 'pre_';

$other_database['slave_status'] = false;
$other_database['slave']['1']['host'] = '192.168.1.11';
$other_database['slave']['1']['username'] = 'root';
$other_database['slave']['1']['password'] = '123456';
$other_database['slave']['1']['port'] = '3306';
$other_database['slave']['1']['database'] = 'discuz';
$other_database['slave']['1']['charset'] = 'utf8';
$other_database['slave']['1']['pconnect'] = 0;
$other_database['slave']['1']['tablepre'] = 'pre_';
$other_database['slave']['1']['weight'] = 0;
</code>