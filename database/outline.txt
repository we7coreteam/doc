=====系统表=====

====core_cache====
系统缓存表

^字段名^数据类型^说明^
|key|varchar(50)|缓存名称|
|value|longtext|缓存内容|

**//操作缓存请使用 [[cache:curd|缓存操作函数]] //**

====core_attachment ====
系统附件表

^字段名^数据类型^说明^
|id|int|主键|
|uniacid|int|所属公众号|
|uid|int|附件所属用户|
|filename|varchar(255)|附件文件名|
|attachment|varchar(255)|附件路径，完整路径请加 ATTACHMENT_ROOT |
|type|tinyint|类型，1为图片，2为音乐，3为视频|
|createtime|int|创建时间|

====core_cron====
系统计划任务表
^字段名^数据类型^说明^
|id|int|主键|
|cloudid|int|新建成功后，云服务返回的任务ID|
|module|varchar(50)|所属模块，system为系统任务|
|uniacid|int|所属公众号|
|type|tinyint|类型，1为定时任务，2为循环任务|
|name|varchar(50)|任务名称|
|filename|varchar(50)|任务执行脚本文件名称，位于模块下方cron目录|
|lastruntime|int|最后一次执行时间|
|nextruntime|int|下一次执行时间|
|weekday|tinyint|周循环，废弃|
|day|tinyint|天循环，废弃|
|hour|tinyint|时循环，废弃|
|minute|varchr(255)|分循环，废弃|
|extra|varchar(5000)|执行结果|
|status|tinyint|执行结果，0成未执行，1为已执行|
|createtime|int|创建时间|