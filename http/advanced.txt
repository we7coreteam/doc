=====获取微信图片=====
微信的图片需要要求必须有引用页，程序中无法直接调用，以下代码实现一个具体引用页的请求来获取微信图片
<code php>
load()->func('communication');
//微信图片
$image = 'https://mmbiz.qlogo.cn/mmbiz_jpg/W0GqhYibias7vXCSLoQicrfiaBJ5bT96UKOMpXDibZdCcJFbCjG30h2Sibxn5HtJp7DZAyOydJ2gttaicMlGfZicrd4yrg/0?wx_fmt=jpeg';
$content = ihttp_request($image, '', array('CURLOPT_REFERER' => 'http://www.qq.com'));
header('Content-Type:image/jpg');
echo $content['content'];
exit();
</code>

=====此实例为获取支付宝的支付地址=====

支付宝的接口通过301跳转来发送给客户端跳转地址，程序中为了获取此url，故设置请求时不自动跳转

<code php>
load()->func('communication');
$response = ihttp_request(ALIPAY_GATEWAY . '?' . http_build_query($set, '', '&'), array(), array('CURLOPT_FOLLOWLOCATION' => 0));
return array('url' => $response['headers']['Location']);
</code>

=====此实例为模拟微信请求地址=====
请求数据为xml格式
<code php>
load()->func('communication');
$response = ihttp_request($item['apiurl'], $message, array('CURLOPT_HTTPHEADER' => array('Content-Type: text/xml; charset=utf-8')));
return $response['content'];
</code>