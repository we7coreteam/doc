=====模板中的一些常用变量=====
模板中提供了一些占位的变量，开发时根据需要可以在代码中定义这些变量，下面将说明有哪些变量
>> 页面标题
<code php>
$_W['page']['title'] = '定义此变量将会改变页面标题栏显示的内容';
</code>

>> 微信分享内容
<code php>
<?php 
$_share = array(
    'title'   => ?,
    'link'    => ?,
    'imgUrl'  => ?,
    'content' => ?
);
</code>