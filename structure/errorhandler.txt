=====错误结构=====
微擎系统只提供了简单的错误结构概念，允许用户进行业务级的错误处理，并不支持系统级的抛出、捕获异常处理。 \\ 
微擎系统很多系统函数也是这样返回错误信息的，在使用时，即量使用 **is_error** 函数来判断

=====使用错误处理器=====
微擎系统中使用 **error($errno, $message = '')** 来定义一个错误结构，使用 **is_error($data)** 函数来判断是否发生错误。例如：
<code php>
$file_delete_status = file_remote_delete('test.jpg');
if (is_error($file_delete_status)) {
	message('删除成功');
} else {
	message('删除失败，错误码如下：' . $file_delete_status['errno'] . '，错误如下：' . $file_delete_status['message']);
}

function file_remote_delete($file) {
	global $_W;
	if(empty($file)) {
		return error(1, '文件不存在');
	}
	$ftp = new Ftp();
	if ($ftp->delete_file($file)) {
		return true;
	} else {
		return error(1, '删除附件失败，请检查配置并重新删除');
	}
}
</code>