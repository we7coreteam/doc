=====总览=====
  * 微擎系统总共分为四大部分“api关键字回复”，“微官网”，“粉丝&会员”，“扩展模块”
  * 各部分功能相互依赖辅助

=====api关键字=====

  * 用于回复粉丝在微信端输入“关键字”后，系统返回其的信息数据，常见的数据有“文本”，“图文”，“语音”，“视频”等
  * 在 1.0 版中，还可以自定义事件用于模块与第三方系统打通。比如要获取会员数据，模块可以接管一个自定义事件叫 we7_event_get_member来获取会员数据
  * 粉丝通过这些回复中的链接可进入系统微站，个人中心或是扩展模块中
  * 也就是说“api关键字、规则回复”驱动了微擎系统的大部分功能

===== 微站=====

  * 微站分为三部分，系统微站（首页）、个人中心及扩展模块本身的微站页面
  * 微站是承载展示、活动及其它活动的一个重要的体现形式

=====扩展模块=====

  * 微擎系统中大部分的营销、活动功能都集中在扩展模块中，在使用时可以随意购买，卸载（要开通云服务）

=====粉丝&会员=====

  * 存储着粉丝&会员的相关资料数据
  * 扩展了积分系统，会员卡系统等重要功能

下面的示意图展示了微擎结构示意图：

{{:dev:structure_1.png|}}